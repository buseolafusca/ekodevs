package com.andela.github.ekodevs.adapter;

import java.io.Serializable;

/**
 * Created by ADEGBUYI on 25-Apr-17.
 */

public interface ListItemClickListener{
    void onClick(Serializable data);
}