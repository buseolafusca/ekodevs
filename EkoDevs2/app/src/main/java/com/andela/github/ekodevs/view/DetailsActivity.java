package com.andela.github.ekodevs.view;

import android.content.Intent;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andela.github.ekodevs.R;
import com.andela.github.ekodevs.data.GitUserDetails;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.user_id_view)
    TextView fullNameView;
    @BindView(R.id.user_handle_view)
    TextView handleView;
    @BindView(R.id.followers_count_view)
    TextView followersCountView;
    @BindView(R.id.share_button)
    Button shareButton;
    @BindView(R.id.repository_count_view)
    TextView repositoryCountView;
    @BindView(R.id.language_view)
    TextView languageView;
    @BindView(R.id.link_view)
    TextView linkView;
    @BindView(R.id.email_view)
    TextView emailView;
    @BindView(R.id.company_view)
    TextView companyView;
    @BindView(R.id.bio_view)
    TextView bioView;
    @BindView(R.id.location_view)
    TextView locationView;
    @BindView(R.id.user_avatar_view)
    ImageView avatarView;

    private GitUserDetails userDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        if (intent.hasExtra(Intent.EXTRA_INTENT)) {
            userDetails = (GitUserDetails) intent.getSerializableExtra(Intent.EXTRA_INTENT);
        }

        setDetails();
    }

    public void setDetails() {
        if (userDetails != null) {
            String fullName = StringUtils.isEmpty(userDetails.getName().replace(" ", "\n")) ? getString(R.string.n_a) : userDetails.getName().replace(" ", "\n");
            String handle = StringUtils.isEmpty(userDetails.getLogin()) ? getString(R.string.n_a) : "@"+userDetails.getLogin();
            String gitUrl = StringUtils.isEmpty(userDetails.getUrl()) ? getString(R.string.n_a) : userDetails.getUrl();
            String email = StringUtils.isEmpty(userDetails.getEmail()) ? getString(R.string.n_a) : userDetails.getEmail();
            String company = StringUtils.isEmpty(userDetails.getCompany()) ? getString(R.string.n_a) : userDetails.getCompany();
            String bio = StringUtils.isEmpty(userDetails.getBio()) ? getString(R.string.n_a) : userDetails.getBio();
            String location = StringUtils.isEmpty(userDetails.getLocation()) ? getString(R.string.n_a) : userDetails.getLocation();

            fullNameView.setText(fullName);
            handleView.setText(handle);
            followersCountView.setText(String.valueOf(userDetails.getFollowers()));
            repositoryCountView.setText(String.valueOf(userDetails.getPublicRepos()));
            languageView.setText(R.string.java);
            linkView.setText(gitUrl);
            emailView.setText(email);
            companyView.setText(company);
            bioView.setText(bio);
            locationView.setText(location);
            Picasso.with(this).load(userDetails.getAvatarUrl()).into(avatarView);

        }
    }

    @OnClick(R.id.share_button)
    public void shareProfile(){
        Intent shareIntent = ShareCompat.IntentBuilder.from(this)
                .setType("text/plain")
                .setChooserTitle(R.string.share_title)
                .setText(getString(R.string.check_this_out) +userDetails.getLogin() +", " + userDetails.getUrl())
                .createChooserIntent();

        if (shareIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(shareIntent);
        }
    }
}
