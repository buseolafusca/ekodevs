package com.andela.github.ekodevs.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.andela.github.ekodevs.R;
import com.andela.github.ekodevs.data.GithubUser;

import java.util.List;


/**
 * Created by ADEGBUYI on 25-Apr-17.
 */

public class PopularUserAdapter extends UserAdapter {

    public PopularUserAdapter(Context context, List<GithubUser> githubUserList, ListItemClickListener listener) {
        super(context, githubUserList, listener);
    }

    @Override
    public PopularUserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.top_user_item_view, parent, false);
        return new PopularUserViewHolder(view, listener);
    }


}