package com.andela.github.ekodevs.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.andela.github.ekodevs.R;
import com.andela.github.ekodevs.data.GithubUser;

import java.util.List;


/**
 * Created by macbookpro on 4/23/17.
 */

public class UserAdapter extends RecyclerView.Adapter<AbstractViewHolder>{

    protected Context context;
    protected List<GithubUser> githubUserList;
    protected ListItemClickListener listener;

    public UserAdapter(Context context, List<GithubUser> githubUserList, ListItemClickListener listener){
        this.context = context;
        this.githubUserList = githubUserList;
        this.listener = listener;
    }

    @Override
    public AbstractViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.user_item_view, parent, false);
        return new UserViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(AbstractViewHolder holder, int position) {
        GithubUser githubUser = githubUserList.get(position);
        holder.bindView(githubUser);
    }

    @Override
    public int getItemCount() {
        return githubUserList.isEmpty() ? 0 : githubUserList.size();
    }



}
