package com.andela.github.ekodevs.data;

import java.io.Serializable;

/**
 * Created by ADEGBUYI on 25-Apr-17.
 */

public class GitUserDetails implements Serializable {

    private String login;
    private int id;
    private String avatar_url;
    private String gravatar_id;
    private String url;
    private String html_url;
    private String followers_url;
    private String following_url;
    private String gists_url;
    private String starred_url;
    private String subscriptions_url;
    private String organizations_url;
    private String repos_url;
    private String events_url;
    private String received_events_url;
    private String type;
    private boolean site_admin;
    private String name;
    private String company;
    private String blog;
    private String location;
    private String email;
    private String bio;
    private int public_repos;
    private int public_gists;
    private int followers;
    private int following;

    public String getLogin() { return this.login; }

    public void setLogin(String login) { this.login = login; }

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    public String getAvatarUrl() { return this.avatar_url; }

    public void setAvatarUrl(String avatar_url) { this.avatar_url = avatar_url; }

    public String getGravatarId() { return this.gravatar_id; }

    public void setGravatarId(String gravatar_id) { this.gravatar_id = gravatar_id; }

    public String getUrl() { return this.url; }

    public void setUrl(String url) { this.url = url; }

    public String getHtmlUrl() { return this.html_url; }

    public void setHtmlUrl(String html_url) { this.html_url = html_url; }

    public String getFollowersUrl() { return this.followers_url; }

    public void setFollowersUrl(String followers_url) { this.followers_url = followers_url; }

    public String getFollowingUrl() { return this.following_url; }

    public void setFollowingUrl(String following_url) { this.following_url = following_url; }

    public String getGistsUrl() { return this.gists_url; }

    public void setGistsUrl(String gists_url) { this.gists_url = gists_url; }

    public String getStarredUrl() { return this.starred_url; }

    public void setStarredUrl(String starred_url) { this.starred_url = starred_url; }

    public String getSubscriptionsUrl() { return this.subscriptions_url; }

    public void setSubscriptionsUrl(String subscriptions_url) { this.subscriptions_url = subscriptions_url; }

    public String getOrganizationsUrl() { return this.organizations_url; }

    public void setOrganizationsUrl(String organizations_url) { this.organizations_url = organizations_url; }

    public String getReposUrl() { return this.repos_url; }

    public void setReposUrl(String repos_url) { this.repos_url = repos_url; }

    public String getEventsUrl() { return this.events_url; }

    public void setEventsUrl(String events_url) { this.events_url = events_url; }

    public String getReceivedEventsUrl() { return this.received_events_url; }

    public void setReceivedEventsUrl(String received_events_url) { this.received_events_url = received_events_url; }

    public String getType() { return this.type; }

    public void setType(String type) { this.type = type; }

    public boolean getSiteAdmin() { return this.site_admin; }

    public void setSiteAdmin(boolean site_admin) { this.site_admin = site_admin; }

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    public String getCompany() { return this.company; }

    public void setCompany(String company) { this.company = company; }

    public String getBlog() { return this.blog; }

    public void setBlog(String blog) { this.blog = blog; }

    public String getLocation() { return this.location; }

    public void setLocation(String location) { this.location = location; }

    public String getEmail() { return this.email; }

    public void setEmail(String email) { this.email = email; }

    public String getBio() { return this.bio; }

    public void setBio(String bio) { this.bio = bio; }

    public int getPublicRepos() { return this.public_repos; }

    public void setPublicRepos(int public_repos) { this.public_repos = public_repos; }

    public int getPublicGists() { return this.public_gists; }

    public void setPublicGists(int public_gists) { this.public_gists = public_gists; }

    public int getFollowers() { return this.followers; }

    public void setFollowers(int followers) { this.followers = followers; }

    public int getFollowing() { return this.following; }

    public void setFollowing(int following) { this.following = following; }

}
