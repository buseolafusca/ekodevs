package com.andela.github.ekodevs.data;

/**
 * Created by macbookpro on 4/23/17.
 */

public class GithubUserContainer
{
    private String incomplete_results;

    private GithubUser[] items;

    private String total_count;

    public String getIncomplete_results ()
    {
        return incomplete_results;
    }

    public void setIncomplete_results (String incomplete_results)
    {
        this.incomplete_results = incomplete_results;
    }

    public GithubUser[] getItems ()
    {
        return items;
    }

    public void setItems (GithubUser[] items)
    {
        this.items = items;
    }

    public String getTotal_count ()
    {
        return total_count;
    }

    public void setTotal_count (String total_count)
    {
        this.total_count = total_count;
    }

}
