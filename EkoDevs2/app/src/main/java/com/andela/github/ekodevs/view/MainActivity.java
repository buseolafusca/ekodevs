package com.andela.github.ekodevs.view;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.andela.github.ekodevs.R;
import com.andela.github.ekodevs.adapter.ListItemClickListener;
import com.andela.github.ekodevs.adapter.PopularUserAdapter;
import com.andela.github.ekodevs.adapter.UserAdapter;
import com.andela.github.ekodevs.data.GitUserDetails;
import com.andela.github.ekodevs.data.GithubUser;
import com.andela.github.ekodevs.data.GithubUserContainer;
import com.andela.github.ekodevs.network.NetworkUtil;
import com.google.gson.Gson;

import java.io.Serializable;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity implements ListItemClickListener {

    @BindView(R.id.top_user_list_view) RecyclerView topUsersRecyclerView;
    @BindView(R.id.user_list_view) RecyclerView userRecyclerView;
    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.container) LinearLayout containerView;

    private LinearLayoutManager verticalLayoutManager;
    private LinearLayoutManager horizontalLayoutManager;
    private List<GithubUser> githubUserList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        verticalLayoutManager = new LinearLayoutManager(this);
        userRecyclerView.setLayoutManager(verticalLayoutManager);

        horizontalLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        topUsersRecyclerView.setLayoutManager(horizontalLayoutManager);

        if(savedInstanceState != null && savedInstanceState.getSerializable(Intent.EXTRA_KEY_EVENT) != null){
            githubUserList =(List<GithubUser>)(savedInstanceState.getSerializable(Intent.EXTRA_KEY_EVENT));
            displayUserList();
        }
        else {
            fetchUsers();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(githubUserList != null){
            outState.putSerializable(Intent.EXTRA_KEY_EVENT, (Serializable)githubUserList);
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null && savedInstanceState.getSerializable(Intent.EXTRA_KEY_EVENT) != null){
            githubUserList =(List<GithubUser>)(savedInstanceState.getSerializable(Intent.EXTRA_KEY_EVENT));
            displayUserList();
        }
    }

    private void fetchUsers() {
        URL url = NetworkUtil.resolveURLWithParams(getString(R.string.git_java_users));

        FetchUsersTask task = new FetchUsersTask();
        task.execute(url);
    }

    private void displayUserList(){
        List<GithubUser> topUsers = githubUserList.subList(0,9);
        List<GithubUser> otherUsers = githubUserList.subList(9, githubUserList.size()-1);

        UserAdapter userAdapter = new UserAdapter(this, otherUsers, this);
        userRecyclerView.setAdapter(userAdapter);
        userAdapter.notifyDataSetChanged();

        PopularUserAdapter popularUserAdapter = new PopularUserAdapter(this, topUsers, this);
        topUsersRecyclerView.setAdapter(popularUserAdapter);
        popularUserAdapter.notifyDataSetChanged();

        progressBar.setVisibility(View.INVISIBLE);
        containerView.setVisibility(View.VISIBLE);
    }

    private void showUserDetails(GitUserDetails gitUserDetails) {
        Intent intent = new Intent(this, DetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Intent.EXTRA_INTENT, gitUserDetails);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private void handleError(){
        Toast.makeText(this, R.string.error_msg_1, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onClick(Serializable data) {
        GithubUser githubUser = (GithubUser)data;
        URL url = NetworkUtil.resolveURLWithParams(githubUser.getUrl());

        FetchUserDetailsTask task = new FetchUserDetailsTask();
        task.execute(url);
    }

    private class FetchUsersTask extends AsyncTask<URL, Void, List<GithubUser>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<GithubUser> doInBackground(URL... urls) {
            try{
                URL url = urls[0];
                String responseString = NetworkUtil.getResponseFromHttpUrl(url);
                GithubUserContainer movieListContainer = new Gson().fromJson(responseString,  GithubUserContainer.class);
                List<GithubUser> githubUsers = Arrays.asList(movieListContainer.getItems());
                return githubUsers;
            }
            catch (Exception ex){
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<GithubUser> githubUsers) {
            super.onPostExecute(githubUsers);
            progressBar.setVisibility(View.INVISIBLE);
            if(githubUsers != null){
                githubUserList = githubUsers;
                displayUserList();
            }
            else{
                handleError();
            }
        }
    }


    private class FetchUserDetailsTask extends AsyncTask<URL, Void, GitUserDetails> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected GitUserDetails doInBackground(URL... urls) {
            try{
                URL url = urls[0];
                String responseString = NetworkUtil.getResponseFromHttpUrl(url);
                GitUserDetails gitUserDetails = new Gson().fromJson(responseString,  GitUserDetails.class);
                return gitUserDetails;
            }
            catch (Exception ex){
                return null;
            }
        }

        @Override
        protected void onPostExecute(GitUserDetails gitUserDetails) {
            super.onPostExecute(gitUserDetails);
            progressBar.setVisibility(View.INVISIBLE);
            if(gitUserDetails != null){
                showUserDetails(gitUserDetails);
            }
            else{
                handleError();
            }
        }
    }

}
