package com.andela.github.ekodevs.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.andela.github.ekodevs.data.GithubUser;


/**
 * Created by ADEGBUYI on 25-Apr-17.
 */

public abstract class AbstractViewHolder  extends RecyclerView.ViewHolder{

    protected final Context context;
    protected ListItemClickListener listener;

    public AbstractViewHolder(View itemView, final ListItemClickListener listener) {
        super(itemView);
        this.listener = listener;
        this.context = itemView.getContext();
    }

    public abstract void bindView(GithubUser githubUser);

}