package com.andela.github.ekodevs.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.andela.github.ekodevs.R;
import com.andela.github.ekodevs.data.GithubUser;
import com.squareup.picasso.Picasso;

/**
 * Created by ADEGBUYI on 25-Apr-17.
 */

class PopularUserViewHolder extends AbstractViewHolder {

    private ImageView avatarView;
    private TextView userIdView;
    private GithubUser githubUser;

    public PopularUserViewHolder(View itemView, final ListItemClickListener listener) {
        super(itemView, listener);
        avatarView = (ImageView) itemView.findViewById(R.id.user_avatar_view);
        userIdView = (TextView) itemView.findViewById(R.id.user_id_view);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listener != null){
                    listener.onClick(githubUser);
                }
            }
        });
    }

    public void bindView(GithubUser githubUser){
        this.githubUser = githubUser;
        if(githubUser != null){
            userIdView.setText(githubUser.getLogin());
            Picasso.with(context).load(githubUser.getAvatar_url()).into(avatarView);
        }
    }
}
